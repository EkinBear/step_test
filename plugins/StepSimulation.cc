#include "StepSimulation.hh"

using namespace gazebo;
GZ_REGISTER_WORLD_PLUGIN(StepSimulation)


// Constructor
StepSimulation::StepSimulation() : WorldPlugin()
{

}

// Deconstructor
StepSimulation::~StepSimulation()
{

}

// Runs once on initialization
void StepSimulation::Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf)
{
  this->world = _parent;
  this->world->SetPaused(true);

  // Initialize ros, if it has not already bee initialized.
  if (!ros::isInitialized())
  {
    int argc = 0;
    char **argv = NULL;
    ros::init(argc, argv, "step_sim",
        ros::init_options::NoSigintHandler);
  }

  // Create our ROS node. This acts in a similar manner to
  // the Gazebo node
  this->rosNode.reset(new ros::NodeHandle());

  std::string topic = "step_sim";
  ros::SubscribeOptions so =
    ros::SubscribeOptions::create<std_msgs::UInt64>(
        topic,
        10,
        boost::bind(&StepSimulation::onStepReceive, this, _1),
        ros::VoidConstPtr(), NULL);

  this->stepSub = this->rosNode->subscribe(so);
}

// Update Callback
void StepSimulation::onStepReceive(const std_msgs::UInt64ConstPtr &msg)
{
  uint64_t step_size = msg->data;
  if (ros::ok())
  {
    ros::spinOnce();
  }
  this->world->Step(step_size);
}
