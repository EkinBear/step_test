#ifndef GAZEBO_PLUGINS_STEP_SIMULATION_PLUGIN_HH_
#define GAZEBO_PLUGINS_STEP_SIMULATION_PLUGIN_HH_

#include <ignition/math/Pose3.hh>
#include "gazebo/physics/physics.hh"
#include <gazebo/sensors/sensors.hh>
#include "gazebo/common/common.hh"
#include "gazebo/gazebo.hh"
#include "sdf/Element.hh"

#include "ros/ros.h"
#include "std_msgs/UInt64.h"
#include "ros/callback_queue.h"
#include "ros/subscribe_options.h"

#include <ignition/transport/Node.hh>
#include <gazebo/transport/Node.hh>

namespace gazebo
{

  class GAZEBO_VISIBLE StepSimulation : public WorldPlugin
  {
    // Constructor
    public: StepSimulation();
    // Deconstructor
    public: virtual ~StepSimulation();

    // Load Function
    // Runs Once on Initialization
    public: virtual void Load(physics::WorldPtr _parent, sdf::ElementPtr _sdf);

    public: void onStepReceive(const std_msgs::UInt64ConstPtr &msg);
    // Pointer to the World
    private: physics::WorldPtr world;
    // Name of models on the initial world
    private: std::vector<std::string> initial_model_names;


    //ROS Node Handler
    private: std::unique_ptr<ros::NodeHandle> rosNode;
    /// \brief A ROS subscriber
    private: ros::Subscriber stepSub;

  };
}
#endif
